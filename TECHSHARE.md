# Techshare Blue Green

## Présenté le 12 juillet 2019 dans l'auditorium de Prisma Media

### Mise en place du déploiement Blue / Green sur les indexeurs

---
name: ticket-1

![Ticket 1](images/ticket-1.jpg)

---
name: do-ticket-1-1

.center[.title[Indexation des articles provenant du Datahub]]

<hr />

#### Première mise en place

<div class="mermaid">
graph RL
    F[Front] -->|index-article| E[ES]
    I(Indexer) -->|index-article| E[ES]
    R(RabbitMQ) -->|CAP_article| I(Indexer)
    BF(Rédaction) --> R(RabbitMQ)
</div>

???

##### Légende

- Rédaction : Personne qui ajoute / édite un article
- RabbitMQ : Simplification du Datahub
- Indexer : Scripts qui enregistrent les informations d'un article
- ES : Elasticsearch
- Front : Site accessible aux internautes

---
name: do-ticket-1-2

.center[.title[Indexation des articles provenant du Datahub]]

<hr />

#### Modifications à cause de l'environnement AWH

<div class="mermaid">
graph RL
    F[Front] -->|index-cap-recette-article| E[ES]
    I(Indexer) -->|index-cap-recette-article| E[ES]
    R(RabbitMQ) -->|CAP_article| I(Indexer)
    BF(Rédaction) --> R(RabbitMQ)
</div>

???

##### Modifications apportées

Le nom de l'index comporte deux nouvelles informations :

- le site (ici <u>cap</u>) car plusieurs index ES de plusieurs sites étaient sur le même cluster
- l'environnement (ici <u>recette</u>) car la recette et la preprod étaient sur le même cluster

---
name: do-ticket-1-3

.center[.title[Indexation des articles provenant du Datahub]]

<hr />

#### Problème 

<div class="mermaid">
graph RL
    F[Front] -->|index-cap-recette-article| E[ES]
    I(Indexer) -->|index-cap-recette-article| E[ES]
    R(RabbitMQ) --> I(Indexer)
    Rx(Réindexation) ==> R(RabbitMQ)
    BF(Rédaction) -.-> R(RabbitMQ)
</div>

???

##### Légende

Une réindexation correspond au renvoi de tous les articles afin qu'ils se fassent (re)traités.

Exemple de scénarios demandant une réindexation :

- changement dans le traitement de l'indexation des articles ou dans le mapping ES
- un nouvel indexeur arrive avec un index inexistant (afin de l'approvisionner)

---
name: ticket-2

![Ticket 2](images/ticket-2.jpg)

---
name: do-ticket-2-1

.center[.title[[Indexation] Fixer l'attente des rédacteurs lorsqu'une réindexation est lancée]]

<hr />

#### Discussions dev <==> infra

--

<u>Exigence</u> : ZDD (Zero Downtime Deployment)

--

<u>Solution</u> : Déploiement Blue / Green

--

<u>Mise en place</u> :

- Côté dev
    - Multiplication par deux du nombre d'index ES (index courant et index futur)
    - Gestion d'alias ES

--

- Côté infra
    - Deux indexeurs plutôt qu'un
    - Gestion DNS (Route 53) pour indiquer quel indexeur est celui actif

---
name: do-ticket-2-2

.center[.title[[Indexation] Fixer l'attente des rédacteurs lorsqu'une réindexation est lancée]]

<hr />

#### 1] Gestion des alias ES

<div class="mermaid">
graph RL
    F[Front] -->|alias-cap-recette-article| E[ES]
    I(Indexer) -->|index-cap-recette-article-version1| E[ES]
    R(RabbitMQ) -->|CAP_article| I(Indexer)
</div>

???

##### Modifications

- Le front pointe dorénavant sur un alias plus sur un index
- L'index se fait suffixer d'un numéro de version (ex: l'identifiant du commit git)

---
name: do-ticket-2-3

.center[.title[[Indexation] Fixer l'attente des rédacteurs lorsqu'une réindexation est lancée]]

<hr />

#### 2] Blue / Green sur les indexeurs

<div class="mermaid">
graph RL
    F[Front] -->|alias-cap-recette-article| E[ES]
    IB(Indexer Blue) -->|index-cap-recette-article-version1| E[ES]
    DNS -.-> IB(Indexer Blue)
    R(RabbitMQ) -->|CAP_blue_article| IB(Indexer Blue)
</div>

???

##### Modifications

- Le nom de l'indexeur comporte dorénavant sa couleur (ici Indexer <u>Blue</u>)
- La queue Rabbit comporte elle aussi la couleur dans son nom (ici CAP_<u>blue</u>_article)
- Le DNS de l'infra fait son apparition précisant quel est l'indexeur actif (couleur active)
    - La couleur active indique qu'au prochain déploiement c'est l'indexeur de l'autre couleur qui se fera écraser

---
name: do-ticket-2-4

.center[.title[[Indexation] Fixer l'attente des rédacteurs lorsqu'une réindexation est lancée]]

<hr />

#### 2] Blue / Green sur les indexeurs (un deuxième indexeur arrive)

<div class="mermaid">
graph RL
    F[Front] -->|alias-cap-recette-article| E[ES]
    IB(Indexer Blue) -->|index-cap-recette-article-version1| E[ES]
    IG(Indexer Green) -->|index-cap-recette-article-version2| E[ES]
    DNS -.-> IB(Indexer Blue)
    R(RabbitMQ) -->|CAP_blue_article| IB(Indexer Blue)
    R(RabbitMQ) ==>|CAP_green_article| IG(Indexer Green)
</div>

???

##### Remarques

- L'alias ne pointera vers <b>index-cap-recette-article-version2</b> qu'une fois tous les articles de ce dernier traités et indexés
- Une fois le switch alias effectué, <b>index-cap-recette-article-version1</b> deviendra orphelin

---
name: do-ticket-2-5

.center[.title[[Indexation] Fixer l'attente des rédacteurs lorsqu'une réindexation est lancée]]

<hr />

#### 3] Quelques nouvelles commandes ajoutées

--

- Suppressions des index orphelins

--

- Switch automatique de l'alias ES

--

- Switch de la couleur active

---
name: do-ticket-2-5

.center[.title[[Indexation] Fixer l'attente des rédacteurs lorsqu'une réindexation est lancée]]

<hr />

#### Allons plus loin

--

Maintenant :

- Alias : `index-cap-recette-article`
- Index : `index-cap-recette-article-version1`

--

Après :

- Alias : `index-cap-recette-article-esSha1`
- Index : `index-cap-recette-article-esSha1-gitSha1`

???

Avec seulement une version de code dans le nom de l'index, nous ne sommes pas en mesure de savoir si le front doit être déployé une fois qu'un indexeur a été déployé et sa réindexation finie.

Si nous séparons la version du mapping ES et la version de code, alors nous saurons que :

- une version de code différente dans un nom d'index ne demandera pas de déploiement du front
- une version de mapping différente demandera un déploiement du front (car des changements seront nécessaires)

---
name: do-ticket-2-6

.center[.title[[Indexation] Fixer l'attente des rédacteurs lorsqu'une réindexation est lancée]]

<hr />

#### 4] Séparation de la version de mapping et de la version de code

<div class="mermaid">
graph RL
    F[Front] -->|alias-cap-recette-article-esSha1| E[ES]
    IB(Indexer Blue) -->|index-cap-recette-article-esSha1-gitSha1| E[ES]
    DNS -.-> IB(Indexer Blue)
    R(RabbitMQ) -->|CAP_blue_article| IB(Indexer Blue)
</div>

???

##### Modification apportée

- La version du mapping ES est maintenant présente dans le nom de l'index

##### Scénario

- L'indexeur <b>Blue</b> a été déployé
- L'index <b>index-cap-recette-article-esSha1-gitSha1</b> a été approvisionné
- L'alias pointe sur <b>index-cap-recette-article-esSha1-gitSha1</b>
- La couleur active a été assignée à <b>Blue</b>

---
name: do-ticket-2-7

.center[.title[[Indexation] Fixer l'attente des rédacteurs lorsqu'une réindexation est lancée]]

<hr />

#### 4] Séparation de la version de mapping et de la version de code

<div class="mermaid">
graph RL
    F[Front] -->|alias-cap-recette-article-esSha1| E[ES]
    IB(Indexer Blue) -->|index-cap-recette-article-esSha1-gitSha1| E[ES]
    IG(Indexer Green) -->|index-cap-recette-article-esSha1-gitSha2| E[ES]
    DNS -.-> IB(Indexer Blue)
    R(RabbitMQ) -->|CAP_blue_article| IB(Indexer Blue)
    R(RabbitMQ) ==>|CAP_green_article| IG(Indexer Green)
</div>

???

##### Scénario

- L'indexeur <b>Green</b> a été déployé
- L'index <b>index-cap-recette-article-esSha1-gitSha2</b> a été approvisionné
- L'alias pointe sur <b>index-cap-recette-article-esSha1-gitSha2</b>

---
name: do-ticket-2-8

.center[.title[[Indexation] Fixer l'attente des rédacteurs lorsqu'une réindexation est lancée]]

<hr />

#### 4] Séparation de la version de mapping et de la version de code

<div class="mermaid">
graph RL
    F[Front] -->|alias-cap-recette-article-esSha1| E[ES]
    IB(Indexer Blue) -->|index-cap-recette-article-esSha1-gitSha1| E[ES]
    IG(Indexer Green) -->|index-cap-recette-article-esSha1-gitSha2| E[ES]
    R(RabbitMQ) -->|CAP_blue_article| IB(Indexer Blue)
    R(RabbitMQ) -->|CAP_green_article| IG(Indexer Green)
    DNS -.-> IG(Indexer Green)
</div>

???

##### Scénario

- La couleur active a été assignée à <b>Green</b>

---
name: do-ticket-2-9

.center[.title[[Indexation] Fixer l'attente des rédacteurs lorsqu'une réindexation est lancée]]

<hr />

#### 4] Séparation de la version de mapping et de la version de code

<div class="mermaid">
graph RL
    F[Front] -->|alias-cap-recette-article-esSha1| E[ES]
    IB(Indexer Blue) -->|index-cap-recette-article-esSha2-gitSha3| E[ES]
    IG(Indexer Green) -->|index-cap-recette-article-esSha1-gitSha2| E[ES]
    R(RabbitMQ) ==>|CAP_blue_article| IB(Indexer Blue)
    R(RabbitMQ) -->|CAP_green_article| IG(Indexer Green)
    DNS -.-> IG(Indexer Green)
</div>

???

##### Scénario

- L'indexeur <b>Blue</b> a été déployé (il écrase Blue car Green est la couleur active)
- L'index <b>index-cap-recette-article-esSha2-gitSha3</b> a été approvisionné

---
name: do-ticket-2-10

.center[.title[[Indexation] Fixer l'attente des rédacteurs lorsqu'une réindexation est lancée]]

<hr />

#### 4] Séparation de la version de mapping et de la version de code

<div class="mermaid">
graph RL
    F[Front] -->|alias-cap-recette-article-esSha2| E[ES]
    IB(Indexer Blue) -->|index-cap-recette-article-esSha2-gitSha3| E[ES]
    IG(Indexer Green) -->|index-cap-recette-article-esSha1-gitSha2| E[ES]
    R(RabbitMQ) -->|CAP_blue_article| IB(Indexer Blue)
    R(RabbitMQ) -->|CAP_green_article| IG(Indexer Green)
    DNS -.-> IG(Indexer Green)
</div>

???

##### Scénario

- Le front a été déployé avec la version de mapping <b>esSha2</b>
- L'alias pointe sur <b>index-cap-recette-article-esSha2-gitSha3</b>

---
name: do-ticket-2-11

.center[.title[[Indexation] Fixer l'attente des rédacteurs lorsqu'une réindexation est lancée]]

<hr />

#### 4] Séparation de la version de mapping et de la version de code

<div class="mermaid">
graph RL
    F[Front] -->|alias-cap-recette-article-esSha2| E[ES]
    IB(Indexer Blue) -->|index-cap-recette-article-esSha2-gitSha3| E[ES]
    IG(Indexer Green) -->|index-cap-recette-article-esSha1-gitSha2| E[ES]
    DNS -.-> IB(Indexer Blue)
    R(RabbitMQ) -->|CAP_blue_article| IB(Indexer Blue)
    R(RabbitMQ) -->|CAP_green_article| IG(Indexer Green)
</div>

???

##### Scénario

- La couleur active a été assignée à <b>Blue</b>

---
name: do-ticket-2-12

.center[.title[Scénarios critiques️]️]

<hr />

--

#### 🔥 Front déployé avant l'indexeur

--

👨‍🚒 Avant que le front soit déployé, il faut savoir si l'alias du front existe et est alimenté

--

<hr />

#### 🔥 Double réindexation

--

👨‍🚒 [Cas exceptionnel] Priorisation des messages

--

<hr />

#### 🔥 Couleur non switchée

--

👨‍🚒 Ouch, pour éviter que ce scénario se (re)produise, il faudrait que la couleur se switche automatiquement

---
name: ticket-3

![Ticket 3](images/ticket-n3.jpg)

---
name: do-ticket-3-1

.center[.title[[Blue / Green] Effectuer le switch de la couleur active automatiquement]]

<hr />

#### Conditions pour le switch automatique

<div class="mermaid">
sequenceDiagram
    participant I as Indexeur Blue ou Green
    participant E as ES
    participant F as Front
    I->>F: Quels sont tes index ?
    F->>E: Quels sont les index de mes alias ?
    E-->>F: Que voici
    F-->>I: Que voici
</div>

--

- Tous les documents sont bien complètement indexés ?
- La version du mapping ES du front et de l'indexeur sont-elles la même ?
- La version de code du front et de l'indexeur sont-elles la même ?

--

Si toutes les conditions sont réunies, l'indexeur fait une demande de switch de couleur (sur sa propre couleur).

---
name: heartbeat

.center[.title[Dernières pensées]]

<hr />

#### Protection grâce aux heartbeat

```json
{
    "color": "green",
    "heartbeatDate": "2019-07-10T12:55:02+02:00",
    "mappingVersion": "6fc3afb9",
    "codeVersion": "1562579089",
    "indices": [{
        "index": "bluegreen-geo-prod-articles-6fc3afb9-1562579089",
        "alias": "bluegreen-geo-prod-articles-6fc3afb9"
    }]
}
```

???

Toutes les 5 minutes, chaque indexeur :

- ajoute un heartbeat
- supprime les heartbeats trop vieux (>= 30 minutes)

Ces heartbeats permettent de savoir si :

- un index ES se fait bien alimenter par un indexeur (une sonde peut être posée)
- ils permettent de connaître la couleur active (sans passer par la console AWS)

---
name: future

.center[.title[Pour le meilleur et le Blue / Green]]

<hr />

#### Idées futures

--

- Brique transverse

--

- Placer l'indexeur ainsi que le front en Blue / Green.

---
name: thanks

.center[.title[Merci]]

<hr />

Url du Techshare : https://gitlab.com/s.robine/techshare-blue-green-deployment

.center[<img width="350" src="images/Came_ha_mais_avec_queur.png" />]

???

#### Bibliothèques utilisées pour le techshare

[[/](https://remarkjs.com)] Remarkjs

[[/](https://mermaidjs.github.io)] Mermaid

#### Musiques écoutées pendant l'écriture

[[/](https://www.youtube.com/watch?v=dy6wBrkhYxk)] Philter - The September Child

[[/](https://www.youtube.com/watch?v=OdNyi1hE650)] Norah's World - Golden Age

[[/](https://www.youtube.com/watch?v=UDKx1Rp1yAA)] Silent Hill 2 OST - True